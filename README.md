# QueueGames.MeanStack

This repository is an upgraded version of [QueueGames repository](https://bitbucket.org/brunoalmada/queuegame/src/master/), using MEAN stack (MongoDB, Express, Angular, NodeJS).

**License**: MIT

### Who do I talk to? ###

* Bruno Almada
* * brunoha@gmail.com
* * [My LinkedIn profile](http://linkedin.com/in/brunoalmada)
* * [My Xing profile](https://www.xing.com/profile/Bruno_HeitzmannAlmada)
* * [about.me](https://about.me/brunoha)